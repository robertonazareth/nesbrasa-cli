/* main.cpp
 *
 * Copyright 2019 Roberto Nazareth <nazarethroberto97@gmail.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <memory>
#include <vector>
#include <string>
#include <sstream>
#include <optional>
#include <fstream>
#include <iterator>
#include "fmt/format.h"
#include "fmt/printf.h"
#include "nesbrasa.hpp"
#include "arquivo.hpp"

using std::vector;
using std::string;
using std::optional;
using std::make_unique;
using nesbrasa::nucleo::Nes;

using namespace nesbrasa::cli;

vector<string> str_separar(const string& s, char delimitador)
{
   std::vector<std::string> tokens;
   std::string token;
   std::istringstream tokenStream(s);
   
   while (std::getline(tokenStream, token, delimitador))
   {
      tokens.push_back(token);
   }

   return tokens;
}

int main(int argc, char** argv)
{
    // Converte os argumentos para um vetor de strings
    vector<string> args;
    for (int i = 0; i < argc; i++)
    {
        args.push_back(argv[i]);
    }


    if (args.size() < 2)
    {
        fmt::print("Erro: nenhum argumento\n");
        return EXIT_FAILURE;
    }

    auto nes = make_unique<Nes>();
    optional<string> caminho = std::nullopt;
    optional<int> endereco = std::nullopt;
    optional<int> ciclos = std::nullopt;
    vector<uint8_t> arquivo;
    
    for (uint32_t i = 1; i < args.size(); i++)
    {
        auto& arg = args[i];

        // checa se a string começa com '--endereco='
        if (arg.rfind("--endereco=", 0) == 0)
        {
            auto arg_separado = str_separar(arg, '=');
            if (arg_separado.size() >= 2)
            {
                try
                {
                    // converter o endereço hexadecimal para um número int
                    endereco = std::stoi(arg_separado[1], nullptr, 16);
                    fmt::print("{}\n", endereco.value());
                }
                catch (...)
                {
                    endereco = std::nullopt;
                    fmt::print("Erro: argumento 'endereco' inválido\n");
                }
            }
        }
        // checa se a string começa com '--ciclos='
        else if (arg.rfind("--ciclos=", 0) == 0)
        {
            auto arg_separado = str_separar(arg, '=');
            if (arg_separado.size() >= 2)
            {
                try
                {
                    // converter a string para um número int
                    ciclos = std::stoi(arg_separado[1]);
                }
                catch (...)
                {
                    ciclos = std::nullopt;
                    fmt::print("Erro: argumento 'loops' inválido\n");
                }
            }
        }
        else 
        {
            if (!caminho.has_value())
            {
                caminho = arg;
            }
        }
    }

    if (!caminho.has_value())
    {
        fmt::print("Erro: O caminho da ROM não foi indicado\n");
        return EXIT_FAILURE;
    }

    try 
    {
        arquivo = ler_arquivo(caminho.value());
    }
    catch (...)
    {
        fmt::print("Erro ao ler arquivo\n");
        return EXIT_FAILURE;
    }

    if (arquivo.size() < 16)
    {
        return EXIT_FAILURE;
    }
    

    for (uint32_t i = 0; i < 16; i++)
    {
        printf("%02X\n", arquivo[i]);
    }

    try 
    {
        nes->carregar_rom(arquivo);
    }
    catch (string err)
    {
        fmt::print("{}\n", err);
        return -1;
    }

    if (endereco.has_value())
    {
        nes->cpu.pc = endereco.value();
    }

    if (!ciclos.has_value())
    {
        ciclos = 300;
    }

    while (nes->cpu.get_ciclos() < ciclos)
    {
        auto opcode = nes->memoria.ler(nes->cpu.pc);
        auto instrucao = nes->cpu.get_instrucao(opcode);

        if (!instrucao.has_value())
        {
            fmt::print("Instrução não reconhecida: {:2X}\n", opcode);
            break;
        }
        
        if (nes->cpu.get_esperar() < 1)
        {
            string linha;
            
            linha += fmt::format("{:04X} | ", nes->cpu.pc);
            linha += nes->cpu.instrucao_para_asm(opcode);

            while (linha.size() < 20)
            {
                linha += " ";
            }
            linha += " | ";
            
            for (uint i = 0; i < instrucao.value().bytes; i++)
            {
                auto valor = nes->memoria.ler(nes->cpu.pc + i);
                linha += fmt::format("{:02X} ", valor);
            }

            while (linha.size() < 32)
            {
                linha += " ";
            }
            linha += " | ";

            linha += fmt::format("a: {:02X} ", nes->cpu.a);
            linha += fmt::format("x: {:02X} ", nes->cpu.x);
            linha += fmt::format("y: {:02X} ", nes->cpu.y);
            linha += fmt::format("n: {:02X} ", nes->cpu.n);
            linha += fmt::format("z: {:02X} ", nes->cpu.z);
            linha += fmt::format("ciclos: {} ", nes->cpu.get_ciclos() + 7);

            fmt::print("{}\n", linha);
        }


        nes->avancar();
    }

    return EXIT_SUCCESS;
}
