#pragma once

#include <fstream>
#include <iterator>
#include <vector>
#include <string>
#include <cstdint>

using std::vector;
using std::string;

namespace nesbrasa::cli
{
    vector<uint8_t> ler_arquivo(string caminho);
}